FROM node:12-alpine
EXPOSE 3000
COPY package.json .
COPY yarn.lock .
RUN yarn
COPY . .
CMD ["yarn", "start"]