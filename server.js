const express = require('express')
const app = express()
const port = 3000

app.get('/liveness', (req, res) => res.json({ status: 'ok' }))
app.get('/helloworld', (req, res) => res.json({ status: 'Hello World' }))
app.get('/newworld', (req, res) => res.json({ status: 'New World' }))

app.listen(port, () => console.log(`Example app listening on port ${port}!`))
